package org.chakra.code;

public class ServiceClass {
	
	private static String OUTPUT = " ";
	
	/*
	 * Method contains the logic of chakra bandha
	 */
	public void ApplyChakraBandha(){
		
		int[] ckr = convert2Dto1DArray(UtilityClass.INPUT);
		
		int rw = 1;
		int cl = 14;
		
		for(int j = 1; j <= 729; j++){
			int ptr = rw * cl;
			int p = ckr[ptr];
			
			System.out.println(15*j + 10 + " " + UtilityClass.DevangiriSymbolTable[p] );
			System.out.println(j + " Row = "+ rw + " Column = "+ cl + " Code = " + ckr[ptr]);
			
			OUTPUT = OUTPUT + UtilityClass.DevangiriSymbolTable[p];
			
			rw = rw - 1;
			cl = cl + 1;
			
			if(rw < 1)
				rw = 27;
			if(cl > 27)
				cl = 1;
		}
		
		System.out.println("The output is " + OUTPUT);
		
	}
	
	/*
	 * method converts 2d array to 1d array
	 */
	public int[] convert2Dto1DArray(int[][] inputArr){
		int[] outputArr = new int[730];
		
		outputArr[0] = 0;
		int counter = 1;
		
		for(int i = 0; i < 27 ; i++){
			for(int j = 0; j < 27; j++){
				outputArr[counter] = inputArr[i][j];
				++ counter ;
			}
		}
		
		return outputArr;
	}

}
